# WarungKu

Tugas besar mobile computing

Anggota Kelompok:
1. M. Faisal Akbar Ritonga  - 1715061001
2. Fathir Rahman D          - 1715061013
 
**Gambaran Applikasi**

<img src="Desain%20Tampilan/1.png"  width="240" height="480">
<img src="Desain%20Tampilan/2.png"  width="240" height="480">
<img src="Desain%20Tampilan/3.png"  width="240" height="480">

Warung Digital merupakan sebuah rancangan applikasi berbasis android/ios yang bertujuan untuk meningkatkan produktifitas dari masyarakat pemilik warung di kompleks-kompleks perumahan atau desa. Warung Digital dirancang juga untuk memudahkan pembeli yang hendak membeli bahan makanan, sembako dan lain-lainnya agar tidak perlu sampai keluar rumah. Dengan Warung Digital harapannya dapat mempermudah penggunanya seperti Ibu Rumah Tangga atau Anak yang disuruh Ibunya pergi ke warung untuk berbelanja ke warung tanpa perlu pergi keluar. Dan untuk pemilik warung agar dagangannya dapat lebih dilihat dan dijangkau masyarakat sehingga keuntungan dari pemilik warung dapat meningkat.

[Desain Tampilan](https://gitlab.com/BeAlph/warungku/-/tree/master/Desain%20Tampilan)

