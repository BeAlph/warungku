// Static
import 'package:WarungKu/colors.dart';
import 'package:WarungKu/login.dart';
import 'package:WarungKu/backdrop.dart';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'model/products_repository.dart';
import 'model/product.dart';


class HomePage extends StatelessWidget {

  // COLLECTIONS STATE
  List<Card> _buildGridCards(BuildContext context) {
    List<Product> products = ProductsRepository.loadProducts(Category.all);

    if (products == null || products.isEmpty) {
      return const <Card>[];
    }

    final ThemeData theme = Theme.of(context);
    final NumberFormat formatter = NumberFormat.simpleCurrency(
        locale: Localizations.localeOf(context).toString());

    return products.map((product) {
      return Card(
        clipBehavior: Clip.antiAlias,
        elevation: 0.0,

        // TODO: Adjust card heights (103)
        child: Column(

          // TODO: Center items on the card (103)
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 18 / 11,

              child: Image.asset(
                product.assetName,

                // TODO: Adjust the box size (102)
                fit: BoxFit.fitWidth,
              ),

            ),
            Expanded(

              child: Padding(
                padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
                child: Column(

                  // TODO: Align labels to the bottom and center (103)
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,

                  // TODO: Change innermost Column (103)
                  children: <Widget>[

                    // TODO: Handle overflowing labels (103)
                    Text(
                      product == null ? '' : product.name,
                      style: theme.textTheme.button,
                      softWrap: false,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),

                    SizedBox(height: 4.0),

                    Text(
                      product == null ? '' : product.alamat,
                      style: theme.textTheme.button,
                      softWrap: false,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),

                  ],

                ),
              ),
            ),
          ],
        ),
      );
    }).toList();
  }

  //
//  List<Card> _buildGridCards(int count) {
//    List<Card> cards = List.generate(
//      count,
//          (int index) => Card(
//        clipBehavior: Clip.antiAlias,
//        child: Column(
//          crossAxisAlignment: CrossAxisAlignment.start,
//          children: <Widget>[
//            AspectRatio(
//              aspectRatio: 18.0 / 11.0,
//              child: Image.asset('assets/diamond.png'),
//            ),
//            Padding(
//              padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Text('Title'),
//                  SizedBox(height: 8.0),
//                  Text('Secondary Text'),
//                ],
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
//
//    return cards;
//  }

  // TODO: Add a variable for Category (104)

  @override
  Widget build(BuildContext context) {
    // TODO: Return an AsymmetricView (104)
    // TODO: Pass Category variable to AsymmetricView (104)

    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              bottom: 70,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Spacer(),
                    Text(
                      "My Profile",
                      style: Theme.of(context)
                          .textTheme
                          .display1
                          .apply(color: orangeMain),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(15.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: orangeLight,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: 15.0,
                            ),
                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 15.0),
                              child: Row(
                                children: <Widget>[
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(5),
                                    child: SizedBox(
                                      child: CircleAvatar(
                                        backgroundImage:
                                        AssetImage('images/ont.jpg'),
                                      ),
                                      width: 80,
                                      height: 80,
                                    ),
                                  ),

                                  SizedBox(
                                    width: 15.0,
                                  ),

                                  Text(
                                    "Tester",
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline
                                        .apply(
                                        color: orangeDeep,
                                        fontWeightDelta: 2),
                                  ),

                                  Spacer(),

                                  IconButton(
                                    icon: Icon(
                                      Icons.settings,
                                      color: orangeDeep,
                                    ),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (BuildContext context) => LoginPage()
                                        )
                                      );
                                    },
                                  )
                                ],
                              ),
                            ),

                            SizedBox(
                              height: 15.0,
                            ),

                            Padding(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 15.0),
                            ),

                            SizedBox(
                              height: 15.0,
                            ),

                            Container(
                              padding: const EdgeInsets.all(25.0),
                              color: orangeDeep,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  DecoratedBox(
                                    child: Icon(
                                      Icons.account_balance_wallet,
                                      size: 30,
                                      color: bgWhite,
                                    ),
                                    decoration: BoxDecoration(),
                                  ),
                                  Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      " 2000 ",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline
                                          .apply(color: Colors.white),
                                    ),
                                  ),
                                  Text(
                                    "Poin",
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline
                                        .apply(color: Colors.white),
                                  ),


                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Spacer(),
                    Text(
                      "Warung Terdekat",
                      style: Theme.of(context)
                          .textTheme
                          .display1
                          .apply(color: orangeDeep),
                    ),

                    SizedBox(
                      height: 15,
                    ),

                    Container(
                      height: MediaQuery.of(context).size.height / 4,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (ctx, i) {
                          return GestureDetector(
                            onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (ctx) => Text("On Progress"),
                              ),
                            ),
                            child: Container(
                              width: 150,
                              margin:
                              const EdgeInsets.symmetric(horizontal: 11.0),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(15.0),
                                child: Stack(
                                  children: _buildGridCards(context)
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
//    return Scaffold(
//      //AppBar
////      appBar: AppBar(
////
////        // Menu
////        leading: IconButton(
////          icon: Icon(
////            Icons.menu,
////            semanticLabel: 'menu',
////          ),
////          onPressed: () {
////            print('Menu button');
////          },
////        ),
////
////        // Tittle
////        centerTitle: true,
////        title: Text("WarungKu"),
////
////        // Trailing Button
////        actions: <Widget>[
////          IconButton(
////            icon: Icon(
////              Icons.search,
////              semanticLabel: 'search',
////            ),
////            onPressed: () {
////              print('Search button');
////            },
////          ),
////          IconButton(
////            icon: Icon(
////              Icons.tune,
////              semanticLabel: 'filter',
////            ),
////            onPressed: () {
////              print('Filter button');
////            },
////          ),
////        ],
////      ),
//
//      // GRID
//      body: GridView.count(
//          crossAxisCount: 2,
//          padding: EdgeInsets.all(16.0),
//          childAspectRatio: 8.0 / 9.0,
//          children: _buildGridCards(context) // Changed code
//      ),
//
//      resizeToAvoidBottomInset: false,
//    );
  }
}

//import 'package:flutter/material.dart';
//
//import 'package:WarungKu/model/products_repository.dart';
//import 'package:WarungKu/model/product.dart';
//
//import 'supplemental/asymmetric_view.dart';
//
//class HomePage extends StatelessWidget {
//
//  final Category category;
//
//  const HomePage({this.category: Category.all});
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: Return an AsymmetricView (104)
//    return  AsymmetricView(products: ProductsRepository.loadProducts(Category.all));

    // Static
//    return Scaffold(
//      appBar: AppBar(
//        brightness: Brightness.light,
//        leading: IconButton(
//          icon: Icon(Icons.menu),
//          onPressed: () {
//            print('Menu button');
//          },
//        ),
//        title: Text('SHRINE'),
//        actions: <Widget>[
//          IconButton(
//            icon: Icon(Icons.search),
//            onPressed: () {
//              print('Search button');
//            },
//          ),
//          IconButton(
//            icon: Icon(Icons.tune),
//            onPressed: () {
//              print('Filter button');
//            },
//          ),
//        ],
//      ),
//      body: AsymmetricView(products: ProductsRepository.loadProducts(Category.all)),
//    );
//  }
//}