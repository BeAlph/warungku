import 'package:flutter/material.dart';

const orangeWhite     = const Color(0xFFffab91);
const orangeLight     = const Color(0xFFff9d3f);
const orangeMain      = const Color(0xFFEF6C00);
const orangeDark      = const Color(0xFFb53d00);
const orangeDeep      = const Color(0xFFe54304);

const errorRed        = const Color(0xFFC5032B);
const bgWhite         = Colors.white;
