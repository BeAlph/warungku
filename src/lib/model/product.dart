import 'package:flutter/foundation.dart';

enum Category { all, accessories, clothing, home }

class Product {
  const Product({
    @required this.category,
    @required this.id,
    @required this.isFeatured,
    @required this.name,
    @required this.price,
    @required this.alamat,
  })  : assert(category != null),
        assert(id != null),
        assert(isFeatured != null),
        assert(name != null),
        assert(price != null),
        assert(alamat !=null);

  final Category category;
  final int id;
  final bool isFeatured;
  final String name;
  final int price;
  final String alamat;

  String get assetName => 'images/$id.jpg';

  @override
  String toString() => "$name (id=$id)";
}
