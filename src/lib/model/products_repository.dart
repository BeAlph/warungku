import 'product.dart';

class ProductsRepository {
  static List<Product> loadProducts(Category category) {
    const allProducts = <Product> [
      Product(
        category: Category.accessories,
        id: 0,
        isFeatured: true,
        name: 'Warung Faiq',
        price: 120,
        alamat: "Jl. Ntah Brantah",
      ),
      Product(
        category: Category.accessories,
        id: 1,
        isFeatured: true,
        name: 'Warung 2',
        price: 58,
        alamat: "Jl. Tak Terbatas",
      ),
      Product(
        category: Category.accessories,
        id: 2,
        isFeatured: false,
        name: 'Warung-warungan',
        price: 35,
        alamat: "Jl. Menuju Kenbenaran",
      ),
    ];
    if (category == Category.all) {
      return allProducts;
    } else {
      return allProducts.where((Product p) {
        return p.category == category;
      }).toList();
    }
  }
}
