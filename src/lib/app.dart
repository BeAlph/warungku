import 'package:flutter/material.dart';

import 'package:WarungKu/colors.dart';
import 'package:WarungKu/backdrop.dart';
import 'package:WarungKu/category_menu_page.dart';
import 'package:WarungKu/home.dart';
import 'package:WarungKu/login.dart';

import 'package:WarungKu/supplemental/cut_corners_border.dart'; //Line 107

import 'package:WarungKu/model/product.dart';

// Show App
class WarungKuApp extends StatefulWidget {
  @override
  _WarungKuAppState createState() => _WarungKuAppState();
}

class _WarungKuAppState extends State<WarungKuApp> {

  Category _currentCategory = Category.all;

  void _onCategoryTap(Category category) {
    setState(() {
      _currentCategory = category;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WarungKu',

      // TODO: Change home: to a Backdrop with a HomePage frontLayer (104)
      home: Backdrop(

        // TODO: Make currentCategory field take _currentCategory (104)
        currentCategory: Category.all,

        // TODO: Pass _currentCategory for frontLayer (104)
        frontLayer: HomePage(),

        // TODO: Change backLayer field value to CategoryMenuPage (104)
        backLayer: CategoryMenuPage(
          currentCategory: _currentCategory,
          onCategoryTap: _onCategoryTap,
        ),

        frontTitle: Text('Test'),
        backTitle: Text('MENU'),

      ),

      initialRoute: '/login',
      onGenerateRoute: _getRoute,

      // TODO: Add a theme (103)
      theme: _colorTheme,
    );
  }

  Route<dynamic> _getRoute(RouteSettings settings) {
    if (settings.name != '/login') {
      return null;
    }

    return MaterialPageRoute<void>(
      settings: settings,
      builder: (BuildContext context) => LoginPage(),
      fullscreenDialog: true,
    );
  }
}

// TODO: Build Theme (103)
final ThemeData _colorTheme = _buildColorTheme();

ThemeData _buildColorTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    accentColor: orangeDeep,
    primaryColor: orangeMain,
    buttonTheme: base.buttonTheme.copyWith(
      buttonColor: orangeDeep,
      colorScheme: base.colorScheme.copyWith(
        secondary: orangeMain,
      ),
    ),
    buttonBarTheme: base.buttonBarTheme.copyWith(
      buttonTextTheme: ButtonTextTheme.accent,
    ),
    scaffoldBackgroundColor : bgWhite,
    cardColor               : bgWhite,
    textSelectionColor      : orangeMain,
    errorColor              : errorRed,

    // TODO: Add the text themes (103)
    textTheme               : _buildShrineTextTheme(base.textTheme),
    primaryTextTheme        : _buildShrineTextTheme(base.primaryTextTheme),
    accentTextTheme         : _buildShrineTextTheme(base.accentTextTheme),

    // TODO: Add the icon themes (103)
    primaryIconTheme: base.iconTheme.copyWith(
        color: Colors.black,
    ),

    // TODO: Decorate the inputs (103)
    inputDecorationTheme: InputDecorationTheme(
      border: CutCornersBorder(),
    ),
  );
}

// TODO: Build a Shrine Text Theme (103)
TextTheme _buildShrineTextTheme(TextTheme base) {

  return base.copyWith(
    headline      : base.headline.copyWith(
      fontWeight  : FontWeight.w500,
    ),

    title: base.title.copyWith(
        fontSize  : 18.0
    ),

    caption: base.caption.copyWith(
      fontWeight  : FontWeight.w400,
      fontSize    : 14.0,
    ),
  ).apply(
    fontFamily    : 'Rubik',
    displayColor  : Colors.black,
    bodyColor     : Colors.black,
  );

}
